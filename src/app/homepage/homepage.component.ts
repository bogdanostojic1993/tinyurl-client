import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { HomepageService } from './homepage.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {

  links;
  long_link:String = '';
  short_link:String = '';
  response = false;
  error:any;
  response_error;
  error_msg = 'Please enter a valid URL.';
  constructor(private homepageService: HomepageService) { }

  ngOnInit(): void {
    this.links = this.homepageService.getTopLinks();
  }

  getLinks() {
    this.links = this.homepageService.getTopLinks()
  }

  postLink(link: String) {
    this.error =null;
    if(!link) return this.error = this.error_msg;
    this.error = false
    this.homepageService.createLink(link)
      .subscribe( (res: any) => {
        this.response = true;
        console.dir(res)
        if(res.code) {
          return this.error = this.error_msg;
        } else {
          this.short_link = res.url;
        }
      })
  }

  resetLink() {
    this.long_link = '';
    this.short_link = '';
    this.response = false;
  }
}
