import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatGridListModule} from '@angular/material/grid-list'; 
import {ClipboardModule} from '@angular/cdk/clipboard'; 

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatGridListModule,
    ClipboardModule

  ]
})
export class HomepageModule { }
