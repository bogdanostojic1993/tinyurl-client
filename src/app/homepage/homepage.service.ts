import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HomepageService {

  readonly POST_URL = 'http://localhost:8080/api/'
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  }
  constructor(private http: HttpClient) { }

  getTopLinks() {
    const BASE_URL = 'https://jsonplaceholder.typicode.com/posts';
    return this.http.get(BASE_URL);
  }

  createLink(input: Object) {
    let params = {
      link: input
    }
    return this.http.post(this.POST_URL, params, this.httpOptions)
  }
}
