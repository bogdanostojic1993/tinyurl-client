import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  readonly GET_URL = `http://localhost:8080/top-urls`;
  constructor(private http: HttpClient) { }

  getTopLinks() {
    return this.http.get(this.GET_URL)
  }
}
