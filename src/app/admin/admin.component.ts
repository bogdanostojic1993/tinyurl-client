import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  links;
  long_link:String = '';
  short_link:String = '';
  response = false;
  error:any;
  response_error;
  error_msg = 'Please enter a valid URL.';
  constructor(private adminServiec: AdminService) { 
    this.adminServiec.getTopLinks()
  }

  ngOnInit(): void {
    this.links = this.adminServiec.getTopLinks();
  }

}
